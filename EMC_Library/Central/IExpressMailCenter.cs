﻿using System.Threading.Tasks;
using EMC_Library.Domain;

namespace EMC_Library.Central
{
    public interface IExpressMailCenter
    {
        string ErrorMessage { get; }
        bool IsSetupOk { get; }
        string SmtpPassword { get; set; }
        int SmtpPort { get; set; }
        string SmtpServerAddress { get; set; }
        string SmtpUsername { get; set; }
        bool SmtpUseSslCertificate { get; set; }

        Task SendEmailWithObject(EmailNotification mailConfigEmailNotification, Recipient userFrom, Recipient userTo,
            string subject, string xHtmlData);

        Task SendEmailWithObject(Recipient userTo, string subject, string xHtmlData);

        void SendEmailWithObjectNoAsync(Recipient userTo, string subject, string xHtmlData);
    }
}