using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Xml.Serialization;
using EMC_Library.Domain;
using EMC_Library.Helpers;
using static System.String;

namespace EMC_Library.Central
{
    public class HtmlEmailTemplateHelper
    {
        internal string ErrorMessage { get; private set; }

        internal string TemplateFolderPath { get; private set; }

        public HtmlEmailTemplateHelper(PathLocationEnum locationType = PathLocationEnum.UseAppSettingFolder)
        {
            ErrorMessage = Empty;

            SetBaseLocation(locationType);

        }

        public T LoadXMLTemplateByType<T>(Dictionary<string, string> dictionary, Enums.XmlHelper.TemplatesEnum classname, String templateName) where T : class
        {

            if (IsNullOrWhiteSpace(TemplateFolderPath))
                throw new ConfigurationErrorsException("EmailTemplateFolder Key/Value is not found in AppSettings's Section config file");


            var pathBase = GetFolderPath();

            string xmlFilePath = $"{pathBase}{classname}\\Config\\{templateName}.xml";
            string htmlFilePath = $"{pathBase}{classname}\\HTML\\{templateName}.html";

            try
            {
                if (!File.Exists(xmlFilePath))
                    throw new FileNotFoundException($"No se encontr� el archivo {templateName}.xml");

                if (!File.Exists(htmlFilePath))
                    throw new FileNotFoundException($"No se encontr� el archivo {htmlFilePath}.html");

                // Hace una instancia del Tipo definido
                var ser = new XmlSerializer(typeof(T));

                Stream srFinal;

                // Leo el archivo XML para poder deserializarlo
                using (var sr = new StreamReader(xmlFilePath))
                {
                    // Lee  el contenido del archivo
                    var data = sr.ReadToEnd();

                    // Parsea la informacion anterior con el diccionario para armar el template final
                    var finalData = EmcHelper.ParseData(data, dictionary);

                    // Arma el archivo 
                    srFinal = EmcHelper.GenerateStreamFromString(finalData);
                }

                T dataFinal = (T)ser.Deserialize(srFinal);

                return dataFinal;
            }
            catch (Exception ex)
            {
                T dataFinal = (T)Activator.CreateInstance(typeof(T));
                ((IErroreable)dataFinal).ErrorMessage = ex.Message;
                return (dataFinal);
            }
        }

        public T LoadXMLTemplateByType<T>(Dictionary<string, string> dictionary, string templateName = "") where T : class
        {
            var pathBase = GetFolderPath();

            string xmlFilePath = $"{pathBase}\\Config\\{templateName}.xml";
            string htmlFilePath = $"{pathBase}\\HTML\\{templateName}.html";


            try
            {
                if (!File.Exists(xmlFilePath))
                    throw new FileNotFoundException($"No se encontr� el archivo {templateName}.xml");

                if (!File.Exists(htmlFilePath))
                    throw new FileNotFoundException($"No se encontr� el archivo {htmlFilePath}.html");

                // Hace una instancia del Tipo definido
                var xmlSerializer = new XmlSerializer(typeof(T));

                // Leo el archivo XML para poder deserializarlo

                Stream streamFromString;

                using (var streamReader = new StreamReader(xmlFilePath))
                {
                    // Lee El total del contenido del archivo
                    var data = streamReader.ReadToEnd();

                    // Parsea la informacion anterior con el diccionario para armar el template final
                    var finalData = EmcHelper.ParseData(data, dictionary);

                    // Arma el archivo 
                    streamFromString = EmcHelper.GenerateStreamFromString(finalData);
                }

                var templateByType = (T)xmlSerializer.Deserialize(streamFromString);

                return templateByType;
            }
            catch (Exception ex)
            {
                var instance = (T)Activator.CreateInstance(typeof(T));

                ((IErroreable)instance).ErrorMessage = ex.Message;

                return instance;
            }
        }

        public string PrepareEmailTemplate(Dictionary<string, string> dictionary, string templateName, Enums.XmlHelper.TemplatesEnum type)
        {
            var pathBase = GetFolderPath();

            string filePath = $"{pathBase}\\{type}\\HTML\\{templateName}.html";

            try
            {
                // Leo el archivo HTML para poder 
                string contentInner;
                using (var htmlStreamReader = new StreamReader(filePath))
                {
                    // Lee el total del contenido del archivo
                    var data = htmlStreamReader.ReadToEnd();

                    // Parsea la informacion anterior con el diccionario para armar el template final
                    // En caso de venir null entonces simplemente pasa lo que encontr�.
                    contentInner = dictionary != null
                        ? EmcHelper.ParseData(data, dictionary)
                        : contentInner = data;
                }

                return contentInner;

            }
            catch (Exception ex)
            {
                ErrorMessage = ex.Message;
                return string.Empty;
            }
        }


        private string GetFolderPath()
        {
            return TemplateFolderPath;
        }

        private void SetBaseLocation(PathLocationEnum locationType, CurrentApplicationType appType = CurrentApplicationType.Web)
        {
            if (locationType == PathLocationEnum.UseCurrentAppFolder)
            {
                if (appType == CurrentApplicationType.Desktop)
                {
                    TemplateFolderPath = Path.GetDirectoryName(Path.GetDirectoryName(Directory.GetCurrentDirectory())) +
                                         "Templates";
                }
                else
                {
                    TemplateFolderPath = AppDomain.CurrentDomain.BaseDirectory + "Templates";

                }
            }
            else
            {
                TemplateFolderPath = ConfigurationManager.AppSettings.Get("EmailTemplateFolder");
            }
        }
    }
}