﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using EMC_Library.Domain;
using EMC_Library.Domain.Enums;
using System.Configuration;
using System.Net.Configuration;
using EMC_Library.Helpers;

namespace EMC_Library.Central
{
    /// <summary>
    /// Permite la capacidad de enviar emails a partir de plantillas HTML y un diccionario de datos en formato KEY / VALUE
    /// </summary>
    public class ExpressMailCenter : IExpressMailCenter
    {
        #region [Members]
        private Encoding MailBodyEncoding { get; }
        public string SmtpServerAddress { get; set; }
        public int SmtpPort { get; set; }
        public string SmtpUsername { get; set; }
        public string SmtpPassword { get; set; }
        public bool SmtpUseSslCertificate { get; set; }

        public string ErrorMessage { get; private set; }

        public bool IsSetupOk { get; private set; }

        private ConfigurationModeEnum _configMode;

        #endregion

        #region [Constructors]

        public ExpressMailCenter(ConfigurationModeEnum configEnum, EncodingMail encodingEmail = EncodingMail.UTF_8, SmtpConfigurationSettings smtpSettings = null)
        {
            _configMode = configEnum;

            MailBodyEncoding = ReturnEncodingMail(encodingEmail);

            if (configEnum == ConfigurationModeEnum.ViaSettingConfigInstance)
            {
                try
                {
                    if (smtpSettings != null)
                    {
                        SmtpServerAddress = smtpSettings.SmtpServerAddress;
                        SmtpPort = smtpSettings.SmtpPortNumber;
                        SmtpUsername = smtpSettings.SmtpUsername;
                        SmtpPassword = smtpSettings.SmtpPassword;
                        SmtpUseSslCertificate = smtpSettings.UseSslCertificate;
                    }

                    IsSetupOk = true;

                }
                catch
                {
                    ErrorMessage = "Please, verifiy  config parameter \"smtpSettings\" are defined";
                    IsSetupOk = false;
                }
            }
            else
            {

                // snip...
                var smtpSection = (SmtpSection)ConfigurationManager.GetSection("system.net/mailSettings/smtp");

                if (smtpSection == null)
                    throw new ConfigurationErrorsException("SMTP Configuration section in Config File is not defined or is not properly defined");

                IsSetupOk = true;
            }


        }

        #endregion

        #region [Methods]

        public static Dictionary<string, string> CreateDictionary(object formItem)
        {
            // Con los datos que me llegan de afuera, establezco el dato necesario para lograr unir XML ←→ Objeto
            var dictionary = EmcHelper.ObjectToDictionary(formItem);

            return dictionary;
        }

        public void SendEmailWithObjectNoAsync(Recipient userTo, string subject, string xHtmlData)
        {
            var smtpSection = (SmtpSection)ConfigurationManager.GetSection("system.net/mailSettings/smtp");

            var from = ConfigurationManager.AppSettings.Get("EmailSenderName");

            var mailConfigEmailNotification = new EmailNotification
            {
                RecipientsList = new List<Recipient> { userTo }
            };


            var fromMail = new MailAddress(smtpSection.From, displayName: from);
            var toMail = new MailAddress(userTo.Email, displayName: userTo.Name );

            MailMessage mailMessage = new MailMessage(fromMail, toMail);
            mailMessage.Subject = subject;


            InternalSendMailNoAsync(mailConfigEmailNotification, xHtmlData, userTo, mailMessage);
        }


        public async Task SendEmailWithObject(Recipient userTo, string subject, string xHtmlData)
        {
            var smtpSection = (SmtpSection)ConfigurationManager.GetSection("system.net/mailSettings/smtp");

            var from = ConfigurationManager.AppSettings.Get("EmailSenderName");

            var mailConfigEmailNotification = new EmailNotification
            {
                RecipientsList = new List<Recipient> { userTo }
            };

            var mailMessage = new MailMessage(
                new MailAddress(smtpSection.From, from),
                new MailAddress(userTo.Email, userTo.Name));
            mailMessage.Subject = subject;


            await InternalSendMail(mailConfigEmailNotification, xHtmlData, userTo, mailMessage);
        }

        public async Task SendEmailWithObject(EmailNotification mailConfigEmailNotification, Recipient userFrom, Recipient userTo, string subject, string xHtmlData)
        {
            var mailMessage = new MailMessage(new MailAddress(userFrom.Email, userFrom.Name), new MailAddress(userTo.Email, userTo.Name));

            mailConfigEmailNotification.Subject = new EmailSubject { Text = subject };

            await InternalSendMail(mailConfigEmailNotification, xHtmlData, userFrom, mailMessage);
        }

        private async Task InternalSendMail(EmailNotification emailNotification, string xHtmlData, Recipient recipient, MailMessage mailMessage)
        {
            try
            {
                var mailsCc = emailNotification.GetUsers(RecipientsEnum.Cc);

                if (mailsCc.Count > 0)
                {
                    var reformattedAddresses = string.Join(",", mailsCc.Select(m => m.Email).ToArray());

                    mailMessage.CC.Add(reformattedAddresses);
                }

                var mailsBcc = emailNotification.GetUsers(RecipientsEnum.Bcc);

                if (mailsBcc.Count > 0)
                {
                    var reformattedAddresses = string.Join(",", mailsBcc.Select(m => m.Email).ToArray());

                    mailMessage.Bcc.Add(reformattedAddresses);
                }

                var mailsReplyTo = emailNotification.GetUsers(RecipientsEnum.ReplyTo);

                if (mailsReplyTo.Count > 0 || (emailNotification.ReplayToLeadOnly && recipient != null))
                {
                    string reformattedAddresses;

                    if (emailNotification.ReplayToLeadOnly && recipient != null)
                        reformattedAddresses = recipient.Email;
                    else
                    {
                        //Por descarte se entiende que MailsReplyTo.Count > 0
                        reformattedAddresses = string.Join(",", mailsReplyTo.Select(m => m.Email).ToArray());
                    }

                    mailMessage.ReplyToList.Add(reformattedAddresses);
                }

                mailMessage.IsBodyHtml = true;
                mailMessage.Body = xHtmlData;
                mailMessage.Subject = mailMessage.Subject ?? emailNotification.Subject.Text;
                mailMessage.Priority = MailPriority.High;
                mailMessage.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;
                mailMessage.BodyEncoding = MailBodyEncoding;


                if (SmtpUseSslCertificate)
                {
                    ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };
                }


                if (_configMode == ConfigurationModeEnum.ViaSettingConfigInstance)
                {
                    using (var smtpClient = new SmtpClient(SmtpServerAddress))
                    {
                        smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;
                        smtpClient.UseDefaultCredentials = false;
                        smtpClient.Credentials = new NetworkCredential(SmtpUsername, SmtpPassword);
                        smtpClient.Port = SmtpPort;


                        await Task.Run(() => smtpClient.SendMailAsync(mailMessage));
                    }
                }
                else
                {
                    using (var smtpClient = new SmtpClient())
                    {
                        smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;

                        var smtpSection = (SmtpSection)ConfigurationManager.GetSection("system.net/mailSettings/smtp");

                        smtpClient.UseDefaultCredentials = false;

                        smtpClient.Credentials = new NetworkCredential(smtpSection.Network.UserName,
                            smtpSection.Network.Password);

                        smtpClient.EnableSsl = smtpSection.Network.EnableSsl;
                        smtpClient.Port = smtpSection.Network.Port;

                        smtpClient.SendMailAsync(mailMessage).Wait();
                    }
                }

                ErrorMessage = string.Empty;
            }
            catch (Exception ex)
            {
                ErrorMessage = ex.InnerException?.Message;
            }
        }


        private void InternalSendMailNoAsync(EmailNotification emailNotification, string xHtmlData, Recipient recipient, MailMessage mailMessage)
        {
            try
            {
                var mailsCc = emailNotification.GetUsers(RecipientsEnum.Cc);

                if (mailsCc.Count > 0)
                {
                    var reformattedAddresses = string.Join(",", mailsCc.Select(m => m.Email).ToArray());

                    mailMessage.CC.Add(reformattedAddresses);
                }

                var mailsBcc = emailNotification.GetUsers(RecipientsEnum.Bcc);

                if (mailsBcc.Count > 0)
                {
                    var reformattedAddresses = string.Join(",", mailsBcc.Select(m => m.Email).ToArray());

                    mailMessage.Bcc.Add(reformattedAddresses);
                }

                var mailsReplyTo = emailNotification.GetUsers(RecipientsEnum.ReplyTo);

                if (mailsReplyTo.Count > 0 || (emailNotification.ReplayToLeadOnly && recipient != null))
                {
                    string reformattedAddresses;

                    if (emailNotification.ReplayToLeadOnly && recipient != null)
                        reformattedAddresses = recipient.Email;
                    else
                    {
                        //Por descarte se entiende que MailsReplyTo.Count > 0
                        reformattedAddresses = string.Join(",", mailsReplyTo.Select(m => m.Email).ToArray());
                    }

                    mailMessage.ReplyToList.Add(reformattedAddresses);
                }

                mailMessage.IsBodyHtml = true;
                mailMessage.Body = xHtmlData;
                mailMessage.Subject = mailMessage.Subject ?? emailNotification.Subject.Text;
                mailMessage.Priority = MailPriority.High;
                mailMessage.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;
                mailMessage.BodyEncoding = MailBodyEncoding;


                if (SmtpUseSslCertificate)
                {
                    ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };
                }


                if (_configMode == ConfigurationModeEnum.ViaSettingConfigInstance)
                {
                    using (var smtpClient = new SmtpClient(SmtpServerAddress))
                    {
                        smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;
                        smtpClient.UseDefaultCredentials = false;
                        smtpClient.Credentials = new NetworkCredential(SmtpUsername, SmtpPassword);
                        smtpClient.Port = SmtpPort;


                        smtpClient.Send(mailMessage);
                    }
                }
                else
                {
                    using (var smtpClient = new SmtpClient())
                    {
                        smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;

                        var smtpSection = (SmtpSection)ConfigurationManager.GetSection("system.net/mailSettings/smtp");

                        smtpClient.UseDefaultCredentials = false;

                        smtpClient.Credentials = new NetworkCredential(smtpSection.Network.UserName,
                            smtpSection.Network.Password);

                        smtpClient.EnableSsl = smtpSection.Network.EnableSsl;
                        smtpClient.Port = smtpSection.Network.Port;

                        smtpClient.Send(mailMessage);
                    }
                }

                ErrorMessage = string.Empty;
            }
            catch (Exception ex)
            {
                ErrorMessage = ex.InnerException?.Message;
            }
        }

        private Encoding ReturnEncodingMail(EncodingMail encodingEmail)
        {
            Encoding enconding;

            switch (encodingEmail)
            {
                case EncodingMail.UTF_8:
                    enconding = Encoding.GetEncoding("utf-8");
                    break;
                case EncodingMail.ISO_8859_1:
                    enconding = Encoding.GetEncoding("iso-8859-1");
                    break;
                default:
                    enconding = Encoding.GetEncoding("utf-8");
                    break;
            }

            return enconding;
        }

        #endregion
    }
}