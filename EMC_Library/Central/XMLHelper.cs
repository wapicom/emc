﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace EMC_Library.Central
{
    public static class XMLHelper
    {
        public enum TemplatesEnum
        {
            AppUser
        }

        public static string ParseData(string data, Dictionary<string, string> replacements)
        {
            foreach (string key in replacements.Keys)
            {
                data = data.Replace(string.Format("{{{0}}}", key), replacements[key]);
            }

            return data;
        }

        public static string SerializeObject(object obj)
        {
            try
            {
                System.Xml.XmlDocument xmlDoc = new System.Xml.XmlDocument();

                System.Xml.Serialization.XmlSerializer serializer = new System.Xml.Serialization.XmlSerializer(obj.GetType());
                using (MemoryStream ms = new MemoryStream())
                {
                    serializer.Serialize(ms, obj);
                    ms.Position = 0;
                    xmlDoc.Load(ms);
                    return xmlDoc.InnerXml;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Ocurrió un error al serializar", ex);
            }
        }

        public static void SaveToXML(string filename, string data)
        {
            try
            {
                using (StreamWriter file = new StreamWriter(filename))
                {
                    file.Write(data);
                }

            }
            catch (Exception)
            {

                throw;
            }

        }

        public static Dictionary<string, string> ObjectToDictionary(object value)
        {
            Dictionary<string, string> dictionary = new Dictionary<string, string>();

            if (value != null)
            {
                foreach (System.ComponentModel.PropertyDescriptor descriptor in System.ComponentModel.TypeDescriptor.GetProperties(value))
                {
                    if (descriptor != null && descriptor.Name != null)
                    {
                        object propValue = descriptor.GetValue(value);
                    
                        dictionary.Add(descriptor.Name, String.Format("{0}", propValue));
                    }
                }
                return dictionary;
            }

            return null;

        }

        public static MemoryStream GenerateStreamFromString(string value)
        {
            return new MemoryStream(Encoding.UTF8.GetBytes(value ?? ""));
        }
    }
}