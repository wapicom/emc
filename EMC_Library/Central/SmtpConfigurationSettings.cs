﻿namespace EMC_Library.Central
{
    public class SmtpConfigurationSettings
    {
        public SmtpConfigurationSettings()
        {
            SmtpPortNumber = 25; // DEFAULT SMTP PORT;
            UseSslCertificate = false;
        }

        public string SmtpServerAddress { get; set; }
        public int SmtpPortNumber { get; set; }
        public string SmtpUsername { get; set; }
        public string SmtpPassword { get; set; }
        public bool UseSslCertificate { get; set; }

    }
}