﻿using System;
using System.Xml.Serialization;
namespace EMC_Library.Domain
{

    [Serializable()]
    [XmlRoot("Configuration")]
    public class TemplatesGenericModel :IErroreable
    {
        [XmlElement("Settings")]
        public Settings Settings { get; set; }

        [XmlElement("EmailNotification")]
        public EmailNotification EmailNotification { get; set; }

        public string XHtmlData { get; set; }
        public string ErrorMessage { get; set; }
    }
}

