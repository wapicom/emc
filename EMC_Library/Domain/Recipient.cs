using EMC_Library.Domain.Enums;
using System;
using System.Xml.Serialization;

namespace EMC_Library.Domain
{
    [Serializable()]
    public class Recipient
    {
        [XmlAttribute("email")]
        public string Email { get; set; }

        [XmlAttribute("name")]
        public string Name { get; set; }

        [XmlAttribute("type")]
        public RecipientsEnum Type { get; set; }

        [XmlAttribute("id")]
        public string Id { get; set; }

        [XmlAttribute("default")]
        public bool Default { get; set; }
    }
}
