using EMC_Library.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;

namespace EMC_Library.Domain
{
    [Serializable()]
    public class EmailNotification
    {
        public EmailNotification()
        {
            
        }
        public EmailNotification(string subject)
        {
            Subject = new EmailSubject { Text = subject };
        }

        //[XmlAttribute("mode")]
        //public ModeEnum Mode { get; set; }

        [XmlAttribute("replaytoleadonly")]
        public bool ReplayToLeadOnly { get; set; }

        [XmlElement("From")]
        public EmailFrom From { get; set; }

        [XmlArrayItem("add", typeof(Recipient))]
        [XmlArray("Recipients")]
        public List<Recipient> RecipientsList { get; set; }

        [XmlElement("Subject")]
        public EmailSubject Subject { get; set; }

        //[XmlElement("AutoResponse")]
        //public AutoResponse AutoResponse { get; set; }

        //[XmlIgnore()]
        //public Recipient ToUser
        //{
        //    get
        //    {
        //        return RecipientsList.FirstOrDefault(m => m.Type == RecipientsEnum.To);
        //    }
        //}

        //[XmlIgnore()]
        //public Recipient UserDefault
        //{
        //    get
        //    {
        //        return RecipientsList.FirstOrDefault(m => m.Default);
        //    }
        //}

        public List<Recipient> GetUsers(RecipientsEnum type)
        {
            return RecipientsList.Where(m => m.Type == type).ToList();
        }

        //public Recipient GetUser(RecipientsEnum type)
        //{
        //    return GetUsers(type).FirstOrDefault();
        //}

        //public Recipient GetUserById(string id)
        //{
        //    return RecipientsList.FirstOrDefault(m => m.Id == id);
        //}
    }
}
