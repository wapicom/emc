using System;
using System.Xml.Serialization;

namespace EMC_Library.Domain
{


    [Serializable()]
    public class FileElement
    {
        [XmlAttribute("name")]
        public string Name { get; set; }
    }

 
}
