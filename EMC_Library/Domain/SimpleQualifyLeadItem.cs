using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace EMC_Library.Domain
{
    #region Settings

    [Serializable()]
    public class SimpleQualifyLeadItem
    {
        [XmlAttribute("type")]
        public string type { get; set; }


        [XmlArrayItem("add", typeof(SimpleRoleItem))]
        [XmlArray("Roles")]
        public List<SimpleRoleItem> Roles { get; set; }
    }


    #endregion
}
