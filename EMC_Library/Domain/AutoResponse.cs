using System;
using System.Xml.Serialization;

namespace EMC_Library.Domain
{

    #region Root

    [Serializable()]
    public class AutoResponse : EmailNotification
    {
        [XmlElement("File")]
        public FileElement File { get; set; }

        public static implicit operator AutoResponse(bool v)
        {
            throw new NotImplementedException();
        }
    }

    #endregion
}
