using System.Collections.Generic;

namespace EMC_Library.Domain
{
    class SitePageComparer : IEqualityComparer<SitePageWrapper>
    {
        // Products are equal if their names and product numbers are equal.
        public bool Equals(SitePageWrapper x, SitePageWrapper y)
        {

            //Check whether the compared objects reference the same data.
            if (ReferenceEquals(x, y)) return true;

            //Check whether any of the compared objects is null.
            if (ReferenceEquals(x, null) || ReferenceEquals(y, null))
                return false;

            //Check whether the products' properties are equal.
            return x.ControllerName.Trim().ToLower() == y.ControllerName.Trim().ToLower() && x.ActionName.Trim().ToLower() == y.ActionName.Trim().ToLower();
        }

        // If Equals() returns true for a pair of objects 
        // then GetHashCode() must return the same value for these objects.

        public int GetHashCode(SitePageWrapper site)
        {
            //Check whether the object is null
            if (ReferenceEquals(site, null)) return 0;

            //Get hash code for the Name field if it is not null.
            int hashProductName = site.ControllerName == null || site.ActionName == null ? 0 : ($"{site.ControllerName}-{site.ActionName}").GetHashCode();

            //Get hash code for the Code field.
            int hashProductCode = ($"{site.ControllerName}-{site.ActionName}").GetHashCode();

            //Calculate the hash code for the product.
            return hashProductName ^ hashProductCode;
        }

    }
}
