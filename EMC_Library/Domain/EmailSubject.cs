using System;
using System.Xml.Serialization;

namespace EMC_Library.Domain
{
    [Serializable()]
    public class EmailSubject
    {
        [XmlAttribute("text")]
        public string Text { get; set; }
    }
}
