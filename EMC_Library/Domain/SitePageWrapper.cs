﻿namespace EMC_Library.Domain
{
    public class SitePageWrapper
    {   
        public string ControllerName { get; set; }

        public string ActionName { get; set; }

        public override string ToString()
        {
            return $"{ControllerName} - {ActionName}";
        }
    }

    
}
