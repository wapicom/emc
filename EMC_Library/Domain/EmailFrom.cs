using System;
using System.Xml.Serialization;

namespace EMC_Library.Domain
{
    [Serializable()]
    public class EmailFrom
    {
        [XmlAttribute("email")]
        public string Email { get; set; }

        [XmlAttribute("name")]
        public string Name { get; set; }
    }
}
