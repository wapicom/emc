namespace EMC_Library.Domain
{
    public interface IErroreable
    {
        string ErrorMessage { get; set; }
    }
}
