﻿namespace EMC_Library.Domain.Enums
{
    public enum ConfigurationModeEnum
    {
        ViaConfigSection,
        ViaSettingConfigInstance
    }
}