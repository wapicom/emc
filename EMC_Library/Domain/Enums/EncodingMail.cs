﻿namespace EMC_Library.Domain.Enums
{
    public enum EncodingMail
    {
        UTF_8,
        ISO_8859_1
    }
}