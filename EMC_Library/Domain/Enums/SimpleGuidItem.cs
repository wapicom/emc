using System;
using System.Xml.Serialization;

namespace EMC_Library.Domain.Enums
{

    [Serializable()]
    public class SimpleGuidItem
    {
        [XmlAttribute("guid")]
        public string Guid { get; set; }
    }
}
