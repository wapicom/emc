using System;
using System.Xml.Serialization;

namespace EMC_Library.Domain.Enums
{

    [Serializable()]
    public class SimpleCodeItem
    {
        [XmlAttribute("code")]
        public string Code { get; set; }
    }
}
