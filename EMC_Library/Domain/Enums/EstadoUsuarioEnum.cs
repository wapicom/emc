namespace EMC_Library.Domain.Enums
{
    public enum EstadoUsuarioEnum
    {
        NoDefinido,
        RegistradoNoActivo,
        RegistradoActivoNoConfirmado,
        RegistradoActivoConfirmadoSinPassword,
        RegistradoActivoConfirmadoOK,
        NoExiste

    }
}
