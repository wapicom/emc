namespace EMC_Library.Domain.Enums
{
    public enum RecipientsEnum
    {
        To,
        Cc,
        Bcc,
        ReplyTo
    }
}
