﻿using System;

namespace EMC_Library.Domain
{
    public class GenericList
    {
        public String Value { get; set; }
        public String Text { get; set; }
        public String ListType { get; set; }
    }
}