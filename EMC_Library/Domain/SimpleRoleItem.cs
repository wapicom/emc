using System;
using System.Xml.Serialization;

namespace EMC_Library.Domain
{
    [Serializable()]
    public class SimpleRoleItem
    {
        [XmlAttribute("ContactRole")]
        public string ContactRole { get; set; }
    }
}
