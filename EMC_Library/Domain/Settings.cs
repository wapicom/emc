using EMC_Library.Domain.Enums;
using System;
using System.Xml.Serialization;

namespace EMC_Library.Domain
{
    [Serializable()]
    public class Settings
    {
        [XmlElement("Proyect")]
        public SimpleGuidItem Proyect { get; set; }

        [XmlElement("Campaign")]
        public SimpleGuidItem Campaign { get; set; }

        [XmlElement("Owner")]
        public SimpleGuidItem Owner { get; set; }

        [XmlElement("CampaignActivity")]
        public SimpleCodeItem CampaignActivity { get; set; }

        [XmlElement("QualifyLead")]
        public SimpleQualifyLeadItem QualifyLeadItem { get; set; }

    }
}
