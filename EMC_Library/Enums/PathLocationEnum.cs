﻿namespace EMC_Library.Central
{
    public enum PathLocationEnum
    {
        UseCurrentAppFolder,
        UseAppSettingFolder
    }

    public enum CurrentApplicationType
    {
        Web,
        Desktop
    }
}