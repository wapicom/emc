﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace EMC_Library.Helpers
{
    public class EmcHelper
    { 
        public static string ParseData(string data, Dictionary<string, string> replacements)
        {
            return replacements.Keys.Aggregate(data, (current, key) => current.Replace($"{{{key}}}", replacements[key]));
        }

        public static string SerializeObject(object obj)
        {
            try
            {
                var xmlDoc = new XmlDocument();

                var serializer = new XmlSerializer(obj.GetType());

                using (var ms = new MemoryStream())
                {
                    serializer.Serialize(ms, obj);
                    ms.Position = 0;
                    xmlDoc.Load(ms);
                    return xmlDoc.InnerXml;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Ocurrió un error al serializar", ex);
            }
        }

        public static void SaveToXML(string filename, string data)
        {
            using (var file = new StreamWriter(filename))
            {
                file.Write(data);
            }
        }

        public static Dictionary<string, string> ObjectToDictionary(object value)
        {
            var dictionary = new Dictionary<string, string>();

            if (value == null) return null;

            for (var i = 0; i < System.ComponentModel.TypeDescriptor.GetProperties(value).Count; i++)
            {
                System.ComponentModel.PropertyDescriptor descriptor =
                    System.ComponentModel.TypeDescriptor.GetProperties(value)[i];

                if (descriptor?.Name == null) continue;

                var propValue = descriptor.GetValue(value);

                dictionary.Add(descriptor.Name, $"{propValue}");
            }

            return dictionary;
        }

        public static MemoryStream GenerateStreamFromString(string value)
        {
            return new MemoryStream(Encoding.UTF8.GetBytes(value ?? ""));
        }
    }
}