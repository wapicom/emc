# Express Mail Center #
Autor: Alfredo Severo

Express Mail Center, (EMC) es una Librería en .NET C# que permite enviar mensajes, basados en templates HTML y datos de configuración.

* Versión 1.00  (13/07/2016)

### Como se configura? ###

* Setup
Se adjunta un cliente de prueba / ejemplo / demo.

* Configuración
Desde el constructor o desde "AppSettings" se setean los parámetros básicos, 
tales como SMTP ADDRESS, PUERTO, usuario y password

* Dependencias?
No tiene dependencias

* Como puedo probarlo?
Simplemente correr EMC_ClientTest

### Como puedo reportar un error o pedir una nueva característica? ###
* contactarme en **digitalports@gmail.com**