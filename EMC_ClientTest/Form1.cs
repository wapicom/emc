﻿using EMC_Library.Central;
using EMC_Library.Domain;
using EMC_Library.Domain.Enums;
using EMC_Library.Enums;
using System;
using System.Windows.Forms;

namespace EMC_ClientTest
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {

            var emailTemplateHelper = new HtmlEmailTemplateHelper();


            string body = emailTemplateHelper.PrepareEmailTemplate(null, "Activation", XmlHelper.TemplatesEnum.AppUser);

            var isOkTemplate = !string.IsNullOrEmpty(body);

            IExpressMailCenter eMailCenter = new ExpressMailCenter(ConfigurationModeEnum.ViaConfigSection, EncodingMail.ISO_8859_1);


            if (!eMailCenter.IsSetupOk || !isOkTemplate)
            {
                return;
            }

            var userTo = new Recipient
            {
                Default = true,
                Id = Guid.NewGuid().ToString(),
                Email = "asevero@wapicom.com",
                Name = "Alfredo",
                Type = RecipientsEnum.To
            };

            eMailCenter.SendEmailWithObject(userTo, "Envio", body);


            if (string.IsNullOrEmpty(eMailCenter.ErrorMessage))
            {
                var resultado = eMailCenter.ErrorMessage;
            }
        }
    }
}
